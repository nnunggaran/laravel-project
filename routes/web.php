<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@home');

Route::get('/register', 'AuthController@register');

Route::post('/welcome', 'AuthController@welcome');

Route::get('/data-table', 'IndexController@table');

//Create Cast
Route::get('/cast/create', 'CastController@create'); //route create cast
Route::post('/cast', 'CastController@store'); //route cast to database

//Read Cast
Route::get('/cast', 'CastController@index'); //route list cast
Route::get('/cast/{cast_id}', 'CastController@show'); //route detail cast

//Update Cast
Route::get('/cast/{cast_id}/edit', 'CastController@edit'); //route edit cast
Route::put('/cast/{cast_id}', 'CastController@update'); //route update cast to database

//Delete Cast
Route::delete('/cast/{cast_id}', 'CastController@destroy'); //route delete data cast
@extends('admin.master')

@section('title')
<p>Halaman Form</p>
@endsection

@section('content')
    <h2>Buat Account Baru</h2>
    <h3>Sign Up Form</h3>

    <form action="/welcome"  method="post">
        @csrf
    <label for="fName">First name:</label><br><br>
    <input type="text" id="fName" name="fName"><br><br>

    <label for="lName">Last name:</label><br><br>
    <input type="text" id="lName" name="lName"><br><br>

    <label for="gender">Gender</label><br><br>
    <input type="radio" id="male" name="gender">Male<br>
    <input type="radio" id="female" name="gender">Female<br><br>

    <label for="nationality">Nationality</label><br>
    <select name="nationality" id="nationality">
        <option value="indonesia">Indonesia</option>
        <option value="indonesia">Foreign</option>
    </select> <br><br>

    <label for="Lspoke">Language Spoken</label><br><br>
    <input type="checkbox" id="bindo" name="Lspoke">Bahasa Indonesia <br>
    <input type="checkbox" id="eng" name="Lspoke">English <br>
    <input type="checkbox" id="other" name="Lspoke">Other <br><br>

    <label for="bio">Bio</label><br>
    <textarea name="" id="" cols="30" rows="10"></textarea><br>
    <input type="submit" value="Sign Up"><br>
    </form>
@endsection